var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('users', {title: 'Pug'})
});

router.get('/cool', (req, res, next) => {
  res.send('You are so cool!');
});

module.exports = router;
